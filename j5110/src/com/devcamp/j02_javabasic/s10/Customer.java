package com.devcamp.j02_javabasic.s10;

public class Customer {
    public int myNum; // integer (whole number)
    public float myFloatNum; // floating point number
    public char myLetter; // character
    public boolean myBool; // boolean
    public String myName; // String
    
    public Customer() {
        myNum = 5;
        myFloatNum = 5.99f;
        myLetter = 'H';
        myBool = true;
        myName = "HieuHN";
    }

    public Customer(int num, float floatNum, char letter, boolean bool, String name) {
        myNum = num;
        myFloatNum = floatNum;
        myLetter = letter;
        myBool = bool;
        myName = name;
    }
}
