package com.devcamp.j02_javabasic.s10;

import java.util.ArrayList;

public class WrapperExample {
    /***
     * Autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang Object
     * của Wrapper class tương ứng
     */
    public static void autoBoxing() {
        char ch = 'a';
        // Autoboxing- primitive to Character object conversion
        Character a = ch;
        // Printing the values from object
        System.out.println(a);

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        // Autoboxing because ArrayList stores only object
        arrayList.add(25);
        // Printing the values from object
        System.out.println(arrayList.get(0));
    }

    /***
     * Unboxing là cơ chế tự động chuyển đổi Object sang kiểu dữ liệu nguyên thủy
     * của Wrapper class tương ứng
     */
    public static void unBoxing() {
        Character ch = 'a';
        // Unboxing- primitive to Character object conversion
        char a = ch;
        // Printing the values from primitive data types
        System.out.println(a);

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(24);
        // Unboxing because get method returns an Integer object
        int num = arrayList.get(0);
        // Printing the values from primitive data types
        System.out.println(num);
    }
}
