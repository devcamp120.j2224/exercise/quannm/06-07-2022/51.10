import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import com.devcamp.j02_javabasic.s10.Customer;
import com.devcamp.j02_javabasic.s10.WrapperExample;

public class App {
    public static void main(String[] args) throws Exception {
        LocalDateTime myDateObj = LocalDateTime.now();
        System.out.println("Before formatting: " + myDateObj);
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        String formattedDate = myDateObj.format(myFormatObj);
        System.out.println("After formatting: " + formattedDate);
        
        App app = new App();
        System.out.println(app.niceDay());
        System.out.println("----------------------");
        System.out.println(app.getVietnamDate());

        System.out.println("----------------------");
        Customer customer = new Customer();
        System.out.println(customer.myNum);
        System.out.println(customer.myFloatNum);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println(customer.myName);

        System.out.println("----------------------");
        customer = new Customer(1, 5.88f, 'G', false, "Devcamp");
        System.out.println(customer.myNum);
        System.out.println(customer.myFloatNum);
        System.out.println(customer.myLetter);
        System.out.println(customer.myBool);
        System.out.println(customer.myName);

        System.out.println("----------------------");
        WrapperExample.autoBoxing();

        System.out.println("----------------------");
        WrapperExample.unBoxing();
    }
    /**
     * 
     * @return String with time format
     */
    public String niceDay() {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Date now = new Date();
        return String.format("Have a nice day. It is %s!.", dateFormat.format(now));
    }

    public String getVietnamDate() {
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("EEEE, dd-MMMM-yyyy")
                                        .localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("Hôm nay là %s! Khuyến mãi ngập tràn.", myFormatObj.format(today));
    }

}
